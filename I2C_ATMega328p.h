/*!
  @file   I2C_ATMega328p.h
  @author  <takuma@VAIO-FIT-15E>
  @date   Tue Dec 27 21:44:42 2016
  
  @brief  ATMega328P用I2C通信ライブラリ
  
  I2C通信でデータをやり取りするライブラリです。

  @version	Alpha 1.0
  @date		2016-12-27	T.Kawamura	新規作成
  @copyright	(C) 2016 T.Kawamura All Rights Reserved.
*/

//#define F_CPU 20000000UL	// 基本的には、MakefileでF_CPUを定義することで二重定義を防ぐ

#ifndef F_SCL
#define F_SCL 100000UL	//! SCLクロック デフォルト値：100KHz
#endif

#ifndef I2C_PRESCALER_VALUE
#define I2C_PRESCALER_VALUE 1	//! I2C分周器 デフォルト値：1
#endif


// TWIビットレートレジスタのマクロ定義
#if I2C_PRESCALER_VALUE == 1
#define TWPS_VALUE 0b00000000
#elif I2C_PRESCALER_VALUE == 4
#define TWPS_VALUE 0b00000001
#elif I2C_PRESCALER_VALUE == 16
#define TWPS_VALUE 0b00000010
#elif I2C_PRESCALER_VALUE == 64
#define TWPS_VALUE 0b00000011
#else
#error "I2C_PRESCALER_VALUE must be 1, 4, 16 or 64!"
#endif
#define TWBR_VALUE ( ( (F_CPU) / (F_SCL) - 16) / (2 * I2C_PRESCALER_VALUE) )

#define TW_STATUS ( TWSR & 0xf8 )

#define MASTER_WRITE 0
#define MASTER_READ 1

void I2C_Init(void);
uint8_t I2C_SendSLA(uint8_t address, uint8_t readOrWrite);
uint8_t I2C_WriteData(uint8_t *dataArray, uint16_t dataSize);
uint8_t I2C_ReadData(uint8_t *dataArray, uint16_t dataSize);
void I2C_Stop(void);
uint8_t I2C_SendAndReceive(uint8_t address, uint8_t *txData, uint8_t txDataSize, uint8_t *rxData, uint8_t rxDataSize );
